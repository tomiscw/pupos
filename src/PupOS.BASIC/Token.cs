// This project licenses this file to you under the Unlicense.
// See the LICENSE file in the project root for more information.
namespace PupOS.BASIC
{
    internal enum Token
    {
        Unknown,

        Identifer,
        Value,

        Compat,
        Print,
        End,

        EOF = -1 //End Of File
    }
}
