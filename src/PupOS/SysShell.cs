// This project licenses this file to you under the Unlicense.
// See the LICENSE file in the project root for more information.
using System;
using PupOS.Common;

namespace PupOS
{
    internal class SysShell : OSShell
    {
        public static void FirstBoot()
        {
            var Bootlines = @"**********************************";
            Console.WriteLine($"{Bootlines}\n{OSCompat.Logo}\n{Bootlines}");
        }
    }
}
