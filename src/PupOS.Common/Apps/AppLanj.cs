// This project licenses this file to you under the Unlicense.
// See the LICENSE file in the project root for more information.
using System;

namespace PupOS.Common.Apps
{
    /// <summary>
    /// A English to Nilagosha dictionary.
    /// </summary>
    class AppLanj : IApp
    {
        public void Entry()
        {
            Core();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd">User input</param>
        static void Dictionary(string cmd)
        {
            switch(cmd)
            {
                case "something":
                case "being":
                case "thing":
                    Print("tise");
                    break;
                case "human":
                case "person":
                    Print("hume");
                    break;
                case "fruit":
                case "vegteable":
                    Print("jumo");
                    break;
                case "document":
                    Print("pagi");
                    break;
                case "women":
                case "female":
                case "woman":
                    Print("womi");
                    break;
                case "strong":
                case "confident":
                case "dependable":
                    Print("sojo");
                    break;
                case "i":
                case "me":
                case "my":
                    Print("koju");
                    break;
                case "male":
                case "man":
                    Print("womi");
                    break;
                case "this":
                case "that":
                    Print("ajo");
                    break;
                case "animal":
                case "land mammal":
                    Print("animu");
                    break;
                case "language":
                    Print("nilago");
                    break;
                case "exit":
                    OSShell.Entry();
                    break;
                default:
                    Console.WriteLine("I didn't understand that.");
                    Core();
                    break;
            }
        }

        /// <summary>
        /// Prints the the word to the screen and returns
        /// to the Core().
        /// </summary>
        /// <param name="word">English word</param>
        static void Print(string word)
        {
            Console.WriteLine(word);
            Core();
        }

        /// <summary>
        /// 
        /// </summary>
        static void Core()
        {
            Console.WriteLine("Type in a word to see the translation. Lowercase only.");
            var input = OSUtil.UserInput(false, "The Language");
            Dictionary(input);
        }
    }
}
