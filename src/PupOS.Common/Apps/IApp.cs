// This project licenses this file to you under the Unlicense.
// See the LICENSE file in the project root for more information.
namespace PupOS.Common.Apps
{
    /// <summary>
    /// 
    /// </summary>
    internal interface IApp
    {
        void Entry();
    }
}
